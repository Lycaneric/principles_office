"""Imports."""
from django.urls import path

from reports import views

app_name = 'reports'
urlpatterns = [
    path('', views.reports_index, name='reports_index'),
    path('<int:report_id>/', views.report, name='report'),
    path('add_officer/<int:report_id>', views.add_officer, name='add_officer'),
    path('make_report/', views.make_report, name='make_report'),
]
