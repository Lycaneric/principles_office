"""Imports."""
from django.contrib.auth.models import User
from django.db import models


class PrinciplesOffice(models.Model):
    """Define a principles office."""

    creator = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        """Give the model a label in admin."""
        return self.name


class UserProfile(models.Model):
    """Define a User."""

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    po = models.ForeignKey(PrinciplesOffice, on_delete=models.CASCADE, null=True, blank=True)
    burner_name = models.CharField(max_length=50)
    picture = models.ImageField(upload_to='user_pics/%Y/%m/%d/')
    about = models.TextField()
    fun_fact = models.CharField(max_length=200)
    email = models.EmailField(max_length=254)
    cell_phone = models.CharField(max_length=15, blank=True)
    show_email = models.BooleanField(default=False)
    show_phone = models.BooleanField(default=False)

    def __str__(self):
        """Give the model a label in admin."""
        return str(self.user)


class Report(models.Model):
    """Define a report."""

    title = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=100)
    updated = models.DateTimeField(auto_now=True)
    po = models.ForeignKey(PrinciplesOffice, on_delete=models.PROTECT,
                           related_name='reports')
    synopsis = models.TextField()
    active = models.BooleanField(default=True)

    def __str__(self):
        """Give the model a label in admin."""
        return self.title + '/' + str(self.po) + '/' + str(self.created)


class ReportAccused(models.Model):
    """Connect accused to a report."""

    report = models.ForeignKey(Report, on_delete=models.CASCADE, related_name='accused')
    first_name = models.CharField(max_length=50)
    alt_first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50)
    alt_last_name = models.CharField(max_length=50, blank=True)
    burner_name = models.CharField(max_length=50)
    alt_burner_name = models.CharField(max_length=50, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    statement = models.TextField(blank=True)

    def __str__(self):
        """Give the model a label in admin."""
        return (str(self.report) + '/' + self.last_name + ', ' + self.first_name)


class ReportAccuser(models.Model):
    """Connect accuser to a report."""

    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    alt_first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50)
    alt_last_name = models.CharField(max_length=50, blank=True)
    burner_name = models.CharField(max_length=50)
    alt_burner_name = models.CharField(max_length=50, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    statement = models.TextField(blank=True)

    def __str__(self):
        """Give the model a label in admin."""
        return (str(self.report) + '/' + self.last_name + ', ' + self.first_name)


class ReportWitness(models.Model):
    """Connect witness to a report."""

    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    alt_first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50)
    alt_last_name = models.CharField(max_length=50, blank=True)
    burner_name = models.CharField(max_length=50)
    alt_burner_name = models.CharField(max_length=50, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    statement = models.TextField(blank=True)

    def __str__(self):
        """Give the model a label in admin."""
        return (str(self.report) + '/' + self.last_name + ', ' + self.first_name)


class ReportOfficer(models.Model):
    """Connect officers to a report."""

    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    officer = models.ForeignKey(User, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    statement = models.TextField(blank=True)
    lead = models.BooleanField(default=False)

    def __str__(self):
        """Give the model a label in admin."""
        return str(self.report) + '/' + str(self.officer)


class ReportImage(models.Model):
    """Connect image to a report."""

    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='report_image/%Y/%m/%d/')
    name = models.CharField(max_length=250, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=250, blank=True)

    def __str__(self):
        """Give the model a label in admin."""
        return str(self.report) + '/' + str(self.created)


class ReportFile(models.Model):
    """Connect file to a report."""

    report = models.ForeignKey(Report, on_delete=models.CASCADE)
    file = models.FileField(upload_to='report_file/%Y/%m/%d/')
    name = models.CharField(max_length=250, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    description = models.CharField(max_length=250, blank=True)

    def __str__(self):
        """Give the model a label in admin."""
        return str(self.report) + '/' + str(self.created)


class Poll(models.Model):
    """Define a poll."""

    text = models.CharField(max_length=250)
    report = models.ForeignKey(Report, on_delete=models.CASCADE,
                               related_name='polls')
    creator = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        """Give the model a label in admin."""
        return str(self.report) + '/' + self.text


class Response(models.Model):
    """Define a response to a poll."""

    poll = models.ForeignKey(Poll, on_delete=models.CASCADE,
                             related_name='responses')
    text = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Give the model a label in admin."""
        return str(self.poll) + '/' + self.text
