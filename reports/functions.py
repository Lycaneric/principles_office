"""Imports."""
from .models import PrinciplesOffice, UserProfile, Report


def FetchContext(request):
    """Fetch info to build context."""
    try:
        user_profile = UserProfile.objects.get(user=request.user)
        po_id = user_profile.po
        po = PrinciplesOffice.objects.get(id=po_id.id)
        active_reports = Report.objects.filter(po=po, active=True)
        reports = Report.objects.filter(po=po)
    except Exception as err:
        print("Couldn't fetch data. " + str(err))
        context = {}
        return context

    context = {
        'po': po,
        'user_profile': user_profile,
        'active_reports': active_reports,
        'reports': reports,
        }
    print(context)
    return context
