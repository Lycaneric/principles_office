"""Imports."""
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import PrinciplesOffice
from .models import UserProfile
from .models import ReportOfficer  # WILLOWISSEXYBEAST


def get_choices(option):
    if option == 'get_users':
        # do stuff
        return


class SignUpForm(UserCreationForm):
    """Layout form."""

    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField(max_length=254)

    class Meta:
        """Define model."""

        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1',
                  'password2', )


class CreatePOForm(forms.ModelForm):
    """Layout form."""

    class Meta:
        """Define model."""

        model = PrinciplesOffice
        fields = ('name',)


class AddOfficerForm(forms.ModelForm):
    def __init__(self, po_id, *args, **kwargs):
        super().__init__(*args, **kwargs)

        CHOICES = (
            ('', ''),
            ('True', 'True'),
            ('False', 'False'),
        )
        print('here')
        print(po_id)
        po = PrinciplesOffice.objects.get(id=po_id)
        user_profile = UserProfile.objects.filter(po=po)
        USER_CHOICES = [
            ('', ''),
        ]
        for user in user_profile:
            usr = User.objects.get(id=user.id)
            USER_CHOICES.append((usr.first_name + ' ' + usr.last_name,
                                usr.first_name + ' ' + usr.last_name))
        self.fields['officer'] = forms.ChoiceField(choices=USER_CHOICES,
                                                   label='Select an officer to add')
        self.fields['lead'] = forms.ChoiceField(widget=forms.CheckboxInput,
                                                choices=CHOICES,
                                                required=False)
