"""Imports."""
from django.contrib import admin

from .models import PrinciplesOffice
from .models import UserProfile
from .models import Report
from .models import ReportAccused
from .models import ReportAccuser
from .models import ReportWitness
from .models import ReportOfficer
from .models import ReportImage
from .models import ReportFile


admin.site.register(PrinciplesOffice)
admin.site.register(UserProfile)
admin.site.register(Report)
admin.site.register(ReportAccused)
admin.site.register(ReportAccuser)
admin.site.register(ReportWitness)
admin.site.register(ReportOfficer)
admin.site.register(ReportImage)
admin.site.register(ReportFile)
