"""Imports."""
from django.contrib.auth import login, authenticate
# from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from .forms import SignUpForm, CreatePOForm, AddOfficerForm
from .models import PrinciplesOffice
from .models import Report
from .models import ReportAccused
from .models import UserProfile
from .models import ReportAccuser
from .models import ReportFile
from .models import ReportImage
from .models import ReportOfficer
from .models import ReportWitness

from . import functions


def index(request):
    """View for the main site index."""
    template = 'reports/index.html'
    if request.user.username == '':
        context = {}
    else:
        context = functions.FetchContext(request)
    return render(request, template, context)


def signup(request):
    """View for signing up."""
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
        return render(request, 'registration/signup.html', {'form': form})


@login_required
def dashboard(request):
    """View for po dashboard."""
    if request.method == 'POST':
        return
    else:
        template = 'reports/dashboard.html'
        context = functions.FetchContext(request)
        return render(request, template, context)


@login_required
def create_po(request):
    """View for creating a po."""
    if request.method == 'POST':
        form = CreatePOForm(request.POST)
        if form.is_valid():
            new_po = form.save(commit=False)
            new_po.creator = request.user
            new_po.save()
            return redirect('dashboard')
    else:
        template = 'reports/create_po.html'
        context = functions.FetchContext(request)
        form = CreatePOForm()
        context['form'] = form
        return render(request, template, context)


@login_required
def po(request, po_id):
    """View for a po."""
    if request.method == 'POST':
        template = ''
        context = {}
        return render(request, template, context)
    else:
        template = 'reports/po.html'
        context = functions.FetchContext(request)
        po = PrinciplesOffice.objects.get(id=po_id)
        officers = []
        members = UserProfile.objects.filter(po=po)
        for member in members:
            officers.append(member.user.get_full_name)
        context['po'] = po
        context['officers'] = officers
        return render(request, template, context)


@login_required
def reports_index(request):
    """View for viewing reports."""
    template = ''
    context = functions.FetchContext(request)
    return render(request, template, context)


@login_required
def report(request, report_id):
    """View for viewing indivual reports."""
    template = 'reports/report.html'
    context = functions.FetchContext(request)
    report = Report.objects.get(id=report_id)
    context['report'] = report
    accused = ReportAccused.objects.filter(report=report).order_by('last_name')
    context['accused'] = accused
    accuser = ReportAccuser.objects.filter(report=report).order_by('last_name')
    context['accuser'] = accuser
    witnesses = ReportWitness.objects.filter(report=report).order_by('last_name')
    context['witnesses'] = witnesses
    officers = ReportOfficer.objects.filter(report=report).order_by('officer')
    context['officers'] = officers
    images = ReportImage.objects.filter(report=report)
    context['images'] = images
    files = ReportFile.objects.filter(report=report)
    context['files'] = files
    return render(request, template, context)


@login_required
def add_officer(request, report_id):
    template = 'reports/add_officer.html'
    context = functions.FetchContext(request)
    if request.method == 'POST':
        form = AddOfficerForm(request.POST)
        if form.is_valid():
            new_officer = form.save(commit=False)
            report = Report.objects.get(id=report_id)
            new_officer.report = report
            return redirect('reports:%s' % report_id)
    else:
        report = Report.objects.get(id=report_id)
        context['form'] = AddOfficerForm()
        context['report'] = report
        return render(request, template, context)


@login_required
def make_report(request):
    """View for creating reports."""
    template = ''
    context = functions.FetchContext(request)
    return render(request, template, context)
